<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
	protected $db = null;
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
			try {
				//Connect to the database
				$db = new PDO('mysql:host=localhost;dbname=test;charset=utf8mb4', 'root');
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->db = $db;
			}
			catch(PDOException $ex) {
				echo "Database error";
			}
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {	
		$booklist = array();
		try{ 
			// Show every book ordered by their id
			$stmt = $this->db->prepare('SELECT * FROM book ORDER BY id');
			$stmt->execute();
			
			// As long as there are more rows in the table
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				// Create a new book to display on the website
				$booklist[] = new Book($row['title'], $row['author'], $row['description'], $row['id']);
			}
		}
		catch(PDOException $ex) {
			echo "Database error";
		}
		
        return $booklist;
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		// If no book is found with the given id a 'null' book will be returned
		$book = null;
		try {
			$stmt = $this->db->prepare("SELECT * FROM book WHERE id = ?");
			$stmt -> execute(array($id));
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			// If book exists in the database
			if ($row) {
				$book = new book($row['title'], $row['author'], $row['description'], $row['id']);
			}
		}
		catch(PDOException $ex) {
			echo "Database error";
		}
        return $book;
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {	
		// Make sure author and title fields are not empty
		if ($book->title != '' && $book->title != null) {
			if ($book->author != '' && $book->author != null) {
				try {
					$stmt = $this->db->prepare("INSERT INTO book (title, author, description) VALUES (?, ?, ?)");
					$stmt -> execute(array("$book->title", "$book->author", "$book->description"));
					
					//Checks that id of added book is valid
					$book->id = $this->db->lastInsertId();
					
					//Description can be a 'null' value
					if ($book->description == '') $book->description = null;
				}
				catch(PDOException $ex) {
					echo "Database error";
				}
			}
		}
	}

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
		// Title and author cannot be emtpy
		if ($book->title != '' && $book->title != null) {
			if ($book->author != '' && $book->author != null) {
				$stmt = $this->db->prepare("UPDATE book SET title= ? , author= ? , description= ? WHERE id = ? ");
				$stmt -> execute(array($book->title, $book->author, $book->description, $book->id));
			}
		}
	}

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
		$stmt = $this->db->prepare("DELETE from book WHERE id = ?");
		$stmt ->execute(array($id));
    }	
}
?>